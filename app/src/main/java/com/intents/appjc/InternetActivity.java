package com.intents.appjc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.appjc.R;

public class InternetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);
    }


    public void onClickBajarImagen(View view) {
        //implícito
        Intent descargarIntent = new Intent(Intent.ACTION_VIEW);
        String imageFile = "https://cdn.icon-icons.com/images/icon-icons.svg";
        descargarIntent.setData(Uri.parse(imageFile));
        startActivity(descargarIntent);
    }
}