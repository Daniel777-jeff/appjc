package com.views.appjc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.appjc.R;
import com.example.appjc.databinding.ActivityVistaBinding;

public class VistaActivity extends AppCompatActivity {
    private static final int DATASET_COUNT = 60;

    private ActivityVistaBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_vista);
        binding = ActivityVistaBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.recyclerViewProductos.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerViewProductos.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new AdaptadorSimple(getData());
        binding.recyclerViewProductos.setAdapter(mAdapter);
    }

    private String[] getData() {
        String[] mDataset = new String[DATASET_COUNT];
        for (int i = 0; i < DATASET_COUNT; i++) {
            mDataset[i] = "Café molido " + i;
        }
        return mDataset;
    }
}