package com.viewscomplex.appjc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.appjc.R;
import com.example.appjc.databinding.ActivityCatalogoBinding;
import com.model.appjc.Producto;
import com.views.appjc.AdaptadorSimple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CatalogoActivity extends AppCompatActivity {

    private ActivityCatalogoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCatalogoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.recyclerViewProductos.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerViewProductos.setLayoutManager(layoutManager);

        List<Producto> productos = getListaProductos();
        RecyclerView.Adapter mAdapter = new AdaptadorProductos(productos);
        binding.recyclerViewProductos.setAdapter(mAdapter);
    }

    protected List<Producto> getListaProductos() {
        List<Producto> productos = new ArrayList<>() ;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("productos.csv")));
            String line;
            Log.e("Reader Stuff",reader.readLine());
            while ((line = reader.readLine()) != null) {
                Log.e("code",line);
                String[] d = line.split(";");

                Producto p = new Producto(Integer.parseInt(d[0]),d[1],Double.parseDouble(d[2]),Double.parseDouble(d[3]),d[4]);
                productos.add(p);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return productos;
    }
}