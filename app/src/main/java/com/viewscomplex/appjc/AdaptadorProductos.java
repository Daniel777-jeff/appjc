package com.viewscomplex.appjc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appjc.R;
import com.model.appjc.Producto;

import java.util.List;


public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.MyViewHolder> {
private List<Producto> productos;
// Provee una referencia a la vista para cada item de datos
public static class MyViewHolder extends RecyclerView.ViewHolder {
    // cada item de datos es solo una cadena para este ejemplo
    public TextView textViewProducto;
    public ImageView imageViewFoto;
    public MyViewHolder(View v) {
        super(v);

        textViewProducto = v.findViewById(R.id.textViewProducto);
        imageViewFoto = v.findViewById(R.id.imageViewFoto);
    }
}

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdaptadorProductos(List<Producto> myDataset) {
        productos = myDataset;
    }

    @Override
    public AdaptadorProductos.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.complex_row_item, parent, false);

        return new AdaptadorProductos.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorProductos.MyViewHolder holder, int position) {
        Producto p = productos.get(position);
        holder.textViewProducto.setText(p.getNombre());
        Glide.with(holder.itemView.getContext()).load(p.getFoto()).into(holder.imageViewFoto);
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}

